/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Context.DBContext;
import Models.Slider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Egamorft
 */
public class SliderDAO {

    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public ArrayList<Slider> getAllSlider() throws SQLException {
        ArrayList<Slider> res = new ArrayList<>();
        try {
            String sql = "SELECT * FROM hr_system_v2.slider where status = 1";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Slider s = new Slider(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6)
                );
                res.add(s);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return res;
    }

    public int getTotalSlider(String query) throws SQLException {
        try {
            String sql = query;
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return -1;
    }
    
    public ArrayList<Slider> getSliderList(String query) throws SQLException {
        ArrayList<Slider> res = new ArrayList<>();
        try {
            String sql = query;
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Slider s = new Slider(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6));
                res.add(s);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return res;
    }
    
    public List<Slider> getOne(int id) {
        List<Slider> list = new ArrayList<>();
        try {
            //mo ket noi
            Connection conn = new DBContext().getConnection();
            String sql = "select * from hr_system_v2.slider where id = ?";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Slider s = new Slider(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6));
                list.add(s);
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        return list;
    }

    public void updateSlider(String image, String title, String backlink, String note, int id) throws SQLException {
        String sql = "UPDATE hr_system_v2.slider SET image = ?, title = ?, backlink = ?, note = ? WHERE id = ?";
        try (
                Connection con = new DBContext().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, image);
            ps.setString(2, title);
            ps.setString(3, backlink);
            ps.setString(4, note);
            ps.setInt(5, id);
            // execute update SQL stetement
            ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }
    
    public void updateSliderImgnull(String title, String backlink, String note, int id) throws SQLException {
        String sql = "UPDATE hr_system_v2.slider SET title = ?, backlink = ?, note = ? WHERE id = ?";
        try (
                Connection con = new DBContext().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, title);
            ps.setString(2, backlink);
            ps.setString(3, note);
            ps.setInt(4, id);
            // execute update SQL stetement
            ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }
    
    public void addnewslider(String image, String title, String backlink, String status, String note) throws SQLException {
        String sql = "INSERT INTO `hr_system_v2`.`slider`\n"
                + "(`image`,\n"
                + "`title`,\n"
                + "`backlink`,\n"
                + "`status`,\n"
                + "`note`)\n"
                + "VALUES (?, ?, ?, ?, ?)";
        try (
                Connection con = new DBContext().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, image);
            ps.setString(2, title);
            ps.setString(3, backlink);
            ps.setString(4, status);
            ps.setString(5, note);
            // execute update SQL stetement
            ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public void editStatus(int status, int id) throws SQLException {
        String sql = "UPDATE `hr_system_v2`.`slider` SET `status` = ? WHERE (`id` = ?);";
        con = new DBContext().getConnection();
        ps = con.prepareStatement(sql);
        if (status == 0) {
            ps.setInt(1, 1);
        } else {
            ps.setInt(1, 0);
        }
        ps.setInt(2, id);
        ps.executeUpdate();
    }
}
