/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Context.DBContext;
import Models.Allocation;
import Models.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author dangGG
 */
public class AllocationDAO {

    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public List<Allocation> getAllocation(String condition1, String condition2, int id, int page) throws SQLException {
        List<Allocation> res = new ArrayList<>();
        try {
            String sql = "SELECT a.id, a.project_code, a.user_id, u.group_code, u.username, u.fullname,a.project_role, a.from, a.to, a.effort, a.notes\n"
                    + "FROM hr_system_v2.allocation as a\n"
                    + "	inner join hr_system_v2.user as u on u.id = a.user_id\n"
                    + "WHERE a.project_code in(\n"
                    + "Select project_code FROM hr_system_v2.allocation \n"
                    + condition1
                    + ") "
                    + condition2
                    + "\n Order by a.project_code"
                    + "\n"
                    + "limit 3 offset ?";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, (page - 1) * 3);
            rs = ps.executeQuery();
            while (rs.next()) {
                Allocation a = new Allocation();
                a.setId(rs.getInt(1));
                a.setProject_code(rs.getString(2));
                a.setUser(new User(rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6)));
                a.setProject_role(rs.getInt(7));
                List<String> from = new ArrayList<>(Arrays.asList(rs.getString(8).split("-")));
                List<String> to = new ArrayList<>(Arrays.asList(rs.getString(9).split("-")));
                Collections.reverse(from);
                Collections.reverse(to);
                a.setFrom(String.join("-", from));
                a.setTo(String.join("-", to));
                a.setEffort(rs.getInt(10));
                a.setNotes(rs.getString(11));
                a.setFrom2(rs.getString(8));
                a.setTo2(rs.getString(9));
                res.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error Allocation: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
        return res;
    }

    public int getTotalAllocation(String condition1, String condition2, int id) throws SQLException {
        try {
            String sql = "SELECT count(*)\n"
                    + "FROM hr_system_v2.allocation as a\n"
                    + "	inner join hr_system_v2.user as u on u.id = a.user_id\n"
                    + "WHERE a.project_code in(\n"
                    + "Select project_code FROM hr_system_v2.allocation \n"
                    + condition1
                    + ") "
                    + condition2
                    + "Order by a.project_code";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("Error Allocation: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
        return -1;
    }

    public void addAllocation(String code, String user_id,
            String role, String from, String to, String effort) throws SQLException {
        try {
            con = new DBContext().getConnection();
            if ("1".equals(role)) {
                int manager_id = getManagerId(code);
                String sql2 = "UPDATE hr_system_v2.allocation a SET a.project_role = 3 WHERE (id = ?)";
                ps = con.prepareStatement(sql2);
                ps.setInt(1, manager_id);
                ps.executeUpdate();
            }
            String sql = "INSERT INTO `hr_system_v2`.`allocation` "
                    + "(`project_code`, `user_id`, `project_role`, `from`, `to`, `effort`) VALUES (?, ?, ?, ?, ?, ?);";
            ps = con.prepareStatement(sql);
            ps.setString(1, code);
            ps.setString(2, user_id);
            ps.setString(3, role);
            ps.setString(4, from);
            ps.setString(5, to);
            ps.setString(6, effort);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error Allocation: " + e.getMessage());
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }

    public int getManagerId(String code) throws SQLException {
        try {
            String sql = "SELECT id FROM hr_system_v2.allocation\n"
                    + "where project_code = ? and project_role = 1;";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, code);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("Error Allocation: " + e.getMessage());
        }
        return 0;
    }

    public void editAllocation(int id, String role, String effort, String from, String to, String note, String project_code) throws SQLException {
        try {
            con = new DBContext().getConnection();
            if ("1".equals(role)) {
                int manager_id = getManagerId(project_code);
                String sql2 = "UPDATE hr_system_v2.allocation a SET a.project_role = 3 WHERE (id = ?)";
                ps = con.prepareStatement(sql2);
                ps.setInt(1, manager_id);
                ps.executeUpdate();
            }
            String sql = "UPDATE `hr_system_v2`.`allocation` SET"
                    + " `project_role` = ?, `from` = ?, `to` = ?, `effort` = ?, `notes` = ? WHERE (`id` = ?);";
            ps = con.prepareStatement(sql);
            ps.setString(1, role);
            ps.setString(4, effort);
            ps.setString(2, from);
            ps.setString(3, to);
            ps.setString(5, note);
            ps.setInt(6, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error Allocation: " + e.getMessage());
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }
    
    public ArrayList<String> getAllUserProjectCode(int id) throws SQLException {
        ArrayList<String> result = new ArrayList<String>();
        try {
            String sql = "select distinct project_code  from hr_system_v2.allocation where user_id = ?";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return result;
    }

    public static void main(String[] args) throws SQLException {
        AllocationDAO aDao = new AllocationDAO();
        aDao.editAllocation(8, "1", "50", "2022-03-17", "2022-03-18", "ok nhe", "ESH");
        //    System.out.println(aDao.getAllocation("", 106, 0));
    }
}
