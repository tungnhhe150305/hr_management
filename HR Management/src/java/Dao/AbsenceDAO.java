/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Context.DBContext;
import Models.Absence;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

/**
 *
 * @author Kha Chinh
 */
public class AbsenceDAO {

    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public Vector<Absence> getListAbsenceByStaff(String sql, int page) throws SQLException {
        Vector vec = new Vector();
        try {
            sql += " limit 5 offset ?";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, (page - 1) * 5);
            rs = ps.executeQuery();
            while (rs.next()) {
                Absence a = new Absence();
                a.setId(rs.getInt(1));
                a.setUser_id(rs.getInt(2));
                a.setTitle(rs.getString(3));
                a.setType(rs.getInt(4));
                a.setStatus(rs.getInt(5));
                a.setRequest_date(rs.getString(6));
                a.setFrom(rs.getString(7));
                a.setTo(rs.getString(8));
                a.setReject_reason(rs.getString(9));
                a.setDuration(rs.getFloat(10));
                vec.add(a);
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return vec;
    }

    public int getTotalAbsenceOfStaff(String sql) throws SQLException {
        int total = 0;
        try {
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                total = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return total;
    }

    public void deleteAbsenceById(int id) throws SQLException {
        try {
            String sql = "DELETE FROM `hr_system_v2`.`absence` WHERE (`id` = ?)";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public void editAbsenceById(int id, String title, int type, String request_date, String from, String to, String duration) throws SQLException {
        try {
            String sql = "UPDATE `hr_system_v2`.`absence` SET `title` = ?, `absence_type` = ?,"
                    + " `status` = '1', `request_date` = ?, `from` = ?, `to` = ?, `reason_reject` = null, "
                    + "`duration` = ? WHERE (`id` = ?);";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, title);
            ps.setInt(2, type);
            ps.setString(3, request_date);
            ps.setString(4, from);
            ps.setString(5, to);
            ps.setString(6, duration);
            ps.setInt(7, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public void addAbsence(int user_id, String title, int type, String request_date, String from, String to, String duration) throws SQLException {
        try {
            String sql = "INSERT INTO `hr_system_v2`.`absence` (`user_id`, `title`, "
                    + "`absence_type`, `status`, `request_date`, `from`, `to`, `duration`)"
                    + " VALUES (?, ?, ?, '1', ?, ?, ?, ?);";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, user_id);
            ps.setString(2, title);
            ps.setInt(3, type);
            ps.setString(4, request_date);
            ps.setString(5, from);
            ps.setString(6, to);
            ps.setString(7, duration);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public String convertFormDate(String dateString) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
        Date date = format1.parse(dateString);
        return format2.format(date);
    }

    public String getGroupByManagerId(int manager_id) throws SQLException {
        try {
            String sql = "SELECT code FROM hr_system_v2.group\n"
                    + "where manager_id = ?";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, manager_id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return null;
    }

    public Vector<Absence> getListAbsenceByManager(String sql, int page) throws SQLException {
        Vector vec = new Vector();
        try {
            sql += " limit 5 offset ?";;
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, (page - 1) * 5);
            rs = ps.executeQuery();
            while (rs.next()) {
                Absence a = new Absence();
                a.setId(rs.getInt(1));
                a.setUser_id(rs.getInt(2));
                a.setTitle(rs.getString(3));
                a.setType(rs.getInt(4));
                a.setStatus(rs.getInt(5));
                a.setRequest_date(rs.getString(6));
                a.setFrom(rs.getString(7));
                a.setTo(rs.getString(8));
                a.setReject_reason(rs.getString(9));
                a.setDuration(rs.getFloat(10));
                a.setGroup(rs.getString(11));
                a.setUser_name(rs.getString(12));
                vec.add(a);
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return vec;
    }

    public Vector<String> getListGroup(String group_code) throws SQLException {
        Vector vec = new Vector();
        try {
            String sql = "SELECT u.group_code\n"
                    + "FROM hr_system_v2.absence a, hr_system_v2.user u\n"
                    + "WHERE u.group_code = ? and u.id = a.user_id\n"
                    + "group by u.group_code";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, group_code);
            rs = ps.executeQuery();
            while (rs.next()) {
                vec.add(rs.getString(1));
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return vec;
    }

    public Vector<String> getListUsername(String group_code) throws SQLException {
        Vector vec = new Vector();
        try {
            String sql = "SELECT u.username\n"
                    + "FROM hr_system_v2.absence a, hr_system_v2.user u\n"
                    + "WHERE u.group_code = ? and u.id = a.user_id\n"
                    + "group by u.username";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, group_code);
            rs = ps.executeQuery();
            while (rs.next()) {
                vec.add(rs.getString(1));
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return vec;
    }

    public void rejectRequest(int id, String reason) throws SQLException {
        try {
            String sql = "UPDATE `hr_system_v2`.`absence` SET `status` = '3', `reject_reason` = ? WHERE (`id` = ?)";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, reason);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public void approveRequest(int id) throws SQLException {
        try {
            String sql = "UPDATE `hr_system_v2`.`absence` SET `status` = '2' WHERE (`id` = ?)";
            con = new DBContext().getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        AbsenceDAO aDAO = new AbsenceDAO();
        System.out.println(aDAO.getGroupByManagerId(107));
    }
}
