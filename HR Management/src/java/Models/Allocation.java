/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author dangGG
 */
public class Allocation {
    private int id;
    private String project_code;
    private int user_id;
    private User user;
    private int project_role;
    private String from;
    private String to;
    private int effort;
    private String notes;
    private String from2;
    private String to2;

    public Allocation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProject_code() {
        return project_code;
    }

    public void setProject_code(String project_code) {
        this.project_code = project_code;
    }

    public int getUser_id() {
        return user_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
   
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getProject_role() {
        return project_role;
    }

    public void setProject_role(int project_role) {
        this.project_role = project_role;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getEffort() {
        return effort;
    }

    public void setEffort(int effort) {
        this.effort = effort;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setFrom2(String from2) {
        this.from2 = from2;
    }

    public void setTo2(String to2) {
        this.to2 = to2;
    }

    public String getFrom2() {
        return from2;
    }

    public String getTo2() {
        return to2;
    }
    
    @Override
    public String toString() {
        return "Allocation{" + "id=" + id + ", project_code=" + project_code + ", project_role=" + project_role + ", from=" + from + ", to=" + to + ", effort=" + effort + ", notes=" + notes + '}';
    }

}
