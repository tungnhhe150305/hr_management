/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Hide
 */
public class FeedBack {
    private String title;
    private String in_charge;
    private String feedback_date;
    private String rate;
    private String comment;

    public FeedBack() {
    }

    public FeedBack(String title, String in_charge, String feedback_date, String rate, String comment) {
        this.title = title;
        this.in_charge = in_charge;
        this.feedback_date = feedback_date;
        this.rate = rate;
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIn_charge() {
        return in_charge;
    }

    public void setIn_charge(String in_charge) {
        this.in_charge = in_charge;
    }

    public String getFeedback_date() {
        return feedback_date;
    }

    public void setFeedback_date(String feedback_date) {
        this.feedback_date = feedback_date;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    
}
