/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author quocb
 */
public class SupportGroup {

    public SupportGroup(int id, int sp_id, String group_code, String des, int staff_id, User fullname) {
        this.id = id;
        this.sp_id = sp_id;
        this.group_code = group_code;
        this.des = des;
        this.staff_id = staff_id;
        this.fullname = fullname;
    }

    public SupportGroup( int staff_id,  User fullname) {
        this.staff_id = staff_id;
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSp_id() {
        return sp_id;
    }

    public void setSp_id(int sp_id) {
        this.sp_id = sp_id;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public User getFullname() {
        return fullname;
    }

    public void setFullname(User fullname) {
        this.fullname = fullname;
    }
    
    private int id;
    private int sp_id;
    private String group_code;
    private String des;
    private int staff_id;
    private User fullname;

    public SupportGroup(int id, int sp_id, String group_code, String des, int staff_id) {
        this.id = id;
        this.sp_id = sp_id;
        this.group_code = group_code;
        this.des = des;
        this.staff_id = staff_id;
    }

   

   

    public SupportGroup() {
    }
}
