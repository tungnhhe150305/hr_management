/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.AllocationDAO;
import Dao.GroupDAO;
import Dao.ProjectDAO;
import Dao.SettingDAO;
import Dao.UserDAO;
import Models.Allocation;
import Models.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dangGG
 */
public class AllocationController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private ProjectDAO projectDAO;
    private GroupDAO groupDAO;
    private AllocationDAO allocationDAO;
    private SettingDAO settingDAO;
    private UserDAO userDAO;

    public void init() {
        groupDAO = new GroupDAO();
        projectDAO = new ProjectDAO();
        allocationDAO = new AllocationDAO();
        settingDAO = new SettingDAO();
        userDAO = new UserDAO();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        try (PrintWriter out = response.getWriter();) {
            String action = request.getPathInfo() == null ? "" : request.getPathInfo();
            String method = request.getMethod();
            switch (action) {
                case "/AllocationList":
                    showHRAllocationView(request, response);
                    break;
                case "/Add":
                    addAllocation(request, response);
                    break;
                case "/Edit":
                    editAllocation(request, response);
                    break;
                default:
                    response.sendError(404);
                    break;
            }
        } catch (Exception ex) {
            log(ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    private void showHRAllocationView(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        User user = (User) request.getSession().getAttribute("account");
        int role = user.getRole_id();
        String condition1 = "";
        String condition2 = "";
        if (role != 3) {
            condition1 = "where user_id = " + user.getId() + " and project_role = 1";
            condition2 = "where manager_id = " + user.getId();
        }
        String condition3 = "";
        String fromDate = request.getParameter("fromDate") != null ? request.getParameter("fromDate") : "";
        String toDate = request.getParameter("toDate") != null ? request.getParameter("toDate") : "";
        String project_code = request.getParameter("project") != null ? request.getParameter("project") : "";
        String group_code = request.getParameter("group") != null ? request.getParameter("group") : "";
        String searchKey = request.getParameter("searchKey") != null ? request.getParameter("searchKey") : "";
        int page = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;

        if (!fromDate.isEmpty()) {
            condition3 += " and a.from >= " + "'" + fromDate + "'";
        }
        if (!toDate.isEmpty()) {
            condition3 += " and a.to <= " + "'" + toDate + "'";
        }
        if (!project_code.isEmpty()) {
            condition3 += " and a.project_code =  " + "'" + project_code + "'";
        }
        if (!group_code.isEmpty()) {
            condition3 += " and u.group_code =  " + "'" + group_code + "'";
        }
        if (!searchKey.isEmpty()) {
            searchKey = String.join(" ", searchKey.split("-"));
            condition3 += " and ( u.username like  " + "'%" + searchKey + "%' OR u.fullname like  " + "'%" + searchKey + "%' )";
        }

        int allocationCount = allocationDAO.getTotalAllocation(condition1, condition3, user.getId());
        int total = allocationCount / 3 + (allocationCount % 3 == 0 ? 0 : 1);
        int begin = 1;
        int end = 3;
        while (page > end) {
            end += 3;
            begin += 3;
        }
        end = Math.min(end, total);
        begin = Math.min(end, begin);
        request.setAttribute("total", total);
        request.setAttribute("begin", begin);
        request.setAttribute("end", end);
        request.setAttribute("currentNumber", page);

        List<Allocation> allocation = allocationDAO.getAllocation(condition1, condition3, user.getId(), page);
        if (allocation.size() != 0) {
            request.setAttribute("allocationList", allocation);
            request.setAttribute("projects", projectDAO.getAllProjectCode(user.getId(), condition2));
            request.setAttribute("groups", groupDAO.getProjectGroupCode(user.getId(), condition1));
            request.setAttribute("projectRoles", settingDAO.getProjectRoles());
        }
        request.getRequestDispatcher("/Views/HRAllocationView.jsp").forward(request, response);
    }

    private void addAllocation(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.getSession().removeAttribute("MessageSuccess");
        request.getSession().removeAttribute("MessageError");
        String id = request.getParameter("user_id");
        String check = userDAO.checkUserId(id);
        if (check == null) {
            String project_code = request.getParameter("project");
            String project_role = request.getParameter("role");
            String effort = request.getParameter("effort");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            allocationDAO.addAllocation(project_code, id, project_role, fromDate, toDate, effort);
            request.getSession().setAttribute("MessageSuccess", "Add successfully!");
        } else {
            request.getSession().setAttribute("MessageError", check);
        }
        response.sendRedirect("/HR_Management/Allocation/AllocationList");
    }

    private void editAllocation(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.getSession().removeAttribute("MessageSuccess");
        request.getSession().removeAttribute("MessageError");
        int id = Integer.parseInt(request.getParameter("id"));
        String project_role = request.getParameter("role");
        String project_code = request.getParameter("project");
        String effort = request.getParameter("effort");
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        String note = request.getParameter("note");
        allocationDAO.editAllocation(id, project_role, effort, fromDate, toDate, note, project_code);
        request.getSession().setAttribute("MessageSuccess", "Edit successfully!");
        response.sendRedirect("/HR_Management/Allocation/AllocationList");
    }
}
