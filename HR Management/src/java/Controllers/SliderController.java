/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.SliderDAO;
import Models.Slider;
import Models.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Egamorft
 */
public class SliderController extends HttpServlet {

    private SliderDAO sliderDAO;

    public void init() {
        sliderDAO = new SliderDAO();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");

        try (PrintWriter out = response.getWriter();) {
            String action = request.getPathInfo() == null ? "" : request.getPathInfo();
            String method = request.getMethod();
            switch (action) {
                case "/List":
                    showSliderView(request, response);
                    break;
                case "/Edit":
                    Edit(request, response, method);
                    break;
                case "/ChangeStatus":
                    changeStatus(request, response);
                    break;
                case "/Add":
                    Add(request, response, method);
                    break;
                default:
                    response.sendError(404);
                    break;
            }
        } catch (Exception ex) {
            log(ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="SliderList">
    private void showSliderView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        User user = (User) request.getSession().getAttribute("account");
        int status = request.getParameter("status") != null ? Integer.parseInt(request.getParameter("status")) : -1;
        String search = request.getParameter("search") != null ? request.getParameter("search") : "";
        int page = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;
        int offset = (page - 1) * 3;

        //this query for search and filter
        String query1 = "SELECT * FROM hr_system_v2.slider where 1=1";
        if (status != -1) {
            query1 += " and status =  " + "'" + status + "'";
        }
        if (!search.isEmpty()) {
            query1 += " and title like  " + "'%" + search + "%' or backlink like " + "'%" + search + "%'";
        }
        query1 += " limit 3 offset " + offset;

        //this query for  total contract
        String query2 = "SELECT count(*) FROM hr_system_v2.slider where 1=1";
        if (status != -1) {
            query2 += " and status =  " + "'" + status + "'";
        }
        if (!search.isEmpty()) {
            query2 += " and title like  " + "'%" + search + "%' or backlink like " + "'%" + search + "%'";
        }
        List<Slider> p = new ArrayList<>();
        int sliderCount = sliderDAO.getTotalSlider(query2);
        int total = sliderCount / 3 + (sliderCount % 3 == 0 ? 0 : 1);
        int begin = 1;
        int end = 3;
        while (page > end) {
            end += 3;
            begin += 3;
        }
        end = Math.min(end, total);
        begin = Math.min(end, begin);
        request.setAttribute("total", total);
        request.setAttribute("begin", begin);
        request.setAttribute("end", end);
        request.setAttribute("currentNumber", page);
        request.setAttribute("sliderList", sliderDAO.getSliderList(query1));
        request.getRequestDispatcher("/Views/SliderList.jsp").forward(request, response);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="SliderEdit">
    private void Edit(HttpServletRequest request, HttpServletResponse response, String method) {
        try (PrintWriter out = response.getWriter();) {
            if (method.equalsIgnoreCase("post")) {
                sliderEditImplement(request, response);
            } else if (method.equalsIgnoreCase("get")) {
                editSliderView(request, response);
            }
        } catch (Exception ex) {
            log(ex.getMessage());
        }
    }

    private void sliderEditImplement(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        String id = request.getParameter("id");
        String image = request.getParameter("fileName");
        String title = request.getParameter("title");
        String backlink = request.getParameter("backlink");
        String note = request.getParameter("note");
        if (image.isEmpty()) {
            sliderDAO.updateSliderImgnull(title, backlink, note, Integer.parseInt(id));
            request.getSession().setAttribute("message", "Edit Slider Successfully!!");
            response.sendRedirect("../Slider/Edit?id=" + id);
        } else {
            sliderDAO.updateSlider(image, title, backlink, note, Integer.parseInt(id));
            request.getSession().setAttribute("message", "Edit Slider Successfully!!");
            response.sendRedirect("../Slider/Edit?id=" + id);

        }
    }

    private void editSliderView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        List<Slider> slider = new SliderDAO().getOne(Integer.parseInt(id));
        request.setAttribute("slider", slider);
        request.getRequestDispatcher("../Views/SliderEdit.jsp").forward(request, response);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="SliderAdd">
    private void Add(HttpServletRequest request, HttpServletResponse response, String method) {
        try (PrintWriter out = response.getWriter();) {
            if (method.equalsIgnoreCase("post")) {
                sliderAddImplement(request, response);
            } else if (method.equalsIgnoreCase("get")) {
                editAddView(request, response);
            }
        } catch (Exception ex) {
            log(ex.getMessage());
        }
    }

    private void sliderAddImplement(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        String image = request.getParameter("fileName");
        String title = request.getParameter("title");
        String backlink = request.getParameter("backlink");
        String note = request.getParameter("note");
        String status = request.getParameter("status");
        if (image.isEmpty()) {
            request.getSession().setAttribute("message", "Image input null !! Pls add one");
            response.sendRedirect("../Slider/Add");
        } else {
            sliderDAO.addnewslider(image, title, backlink, status, note);
            request.getSession().setAttribute("message", "Add new Slider successfully !!");
            response.sendRedirect("../Slider/Add");
        }
    }

    private void editAddView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("../Views/SliderAdd.jsp").forward(request, response);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="changeStatus">
    private void changeStatus(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        String st = request.getParameter("status");
        String id = request.getParameter("id");
        sliderDAO.editStatus(Integer.parseInt(st), Integer.parseInt(id));
        int page = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;
        int begin = 1;
        int end = 3;
        while (page > end) {
            end += 3;
            begin += 3;
        }
        request.setAttribute("currentNumber", page);
        response.sendRedirect("Slider/List");
    }
    // </editor-fold>

}
