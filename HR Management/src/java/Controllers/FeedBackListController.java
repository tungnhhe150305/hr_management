/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.FeedBackDAO;
import Dao.GroupDAO;
import Dao.ProjectDAO;
import Dao.SettingDAO;
import Dao.TimesheetDAO;
import Dao.UserDAO;
import Models.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hide
 */
public class FeedBackListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private TimesheetDAO timesheetDAO;
    private SettingDAO settingDAO;
    private ProjectDAO projectDAO;
    private UserDAO userDAO;
    private FeedBackDAO feedbackDAO;
    private GroupDAO groupDAO;

    public void init() {
        timesheetDAO = new TimesheetDAO();
        settingDAO = new SettingDAO();
        projectDAO = new ProjectDAO();
        userDAO = new UserDAO();
        feedbackDAO = new FeedBackDAO();
        groupDAO = new GroupDAO();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter();) {
            String action = request.getPathInfo() == null ? "" : request.getPathInfo();
            String method = request.getMethod();
            switch (action) {
                case "/FeedBackList":
                    showFeedBackListListView(request, response);
                    break;
                default:
                    response.sendError(404);
                    break;
            }
        } catch (Exception ex) {
            log(ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void showFeedBackListListView(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        User user = (User) request.getSession().getAttribute("account");
        String fromDate = request.getParameter("fromDate") != null ? request.getParameter("fromDate") : "";
        String toDate = request.getParameter("toDate") != null ? request.getParameter("toDate") : "";
        String incharge = request.getParameter("incharge") != null ? request.getParameter("incharge") : "";
        String rate = request.getParameter("rate") != null ? request.getParameter("rate") : "";
        String title = request.getParameter("title") != null ? request.getParameter("title") : "";
        int process = request.getParameter("process") != null ? Integer.parseInt(request.getParameter("process")) : 0;
        int page = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;
        int offset = (page - 1) * 3;

        //this query for search and filter
        String query1 = "SELECT * FROM hr_system_v2.request Where in_charge_staff= " + user.getId();
        if (!fromDate.isEmpty()) {
            query1 += " and request_date >= " + "'" + fromDate + "'";
        }
        if (!toDate.isEmpty()) {
            query1 += " and update_date <= " + "'" + toDate + "'";
        }
        if (!incharge.isEmpty()) {
            query1 += " and in_charge_group =  " + "'" + incharge + "'";
        }
        if (!title.isEmpty()) {
            query1 += " and title like  " + "'%" + title + "%'";
        }
        if (!rate.isEmpty()) {
            query1 += " and rating =  " + "'" + rate + "'";
        }
        query1 += " limit 3 offset " + offset;

        //this query for  total timesheet
        String query2 = "SELECT count(id) FROM hr_system_v2.request Where in_charge_staff= " + user.getId();
        if (!fromDate.isEmpty()) {
            query2 += " and request_date >= " + "'" + fromDate + "'";
        }
        if (!toDate.isEmpty()) {
            query2 += " and update_date <= " + "'" + toDate + "'";
        }
        if (!incharge.isEmpty()) {
            query2 += " and in_charge_group =  " + "'" + incharge + "'";
        }
        if (!title.isEmpty()) {
            query2 += " and title like  " + "'%" + title + "%'";
        }
        if (!rate.isEmpty()) {
            query2 += " and rating =  " + "'" + rate + "'";
        }

        int fbCoun1t = feedbackDAO.getTotalFeedBack(query1);
        int fbCount = feedbackDAO.getTotalFeedBack(query2);
        int total = fbCount / 3 + (fbCount % 3 == 0 ? 0 : 1);
        int begin = 1;
        int end = 3;
        while (page > end) {
            end += 3;
            begin += 3;
        }
        end = Math.min(end, total);
        begin = Math.min(end, begin);
        request.setAttribute("total", total);
        request.setAttribute("begin", begin);
        request.setAttribute("end", end);
        request.setAttribute("currentNumber", page);
        request.setAttribute("timesheetList", feedbackDAO.getFeedBackList(query1));
        request.setAttribute("groups", groupDAO.getAllGroupCode());
        request.setAttribute("projects", projectDAO.getAllProjectCode(user.getGroup_code()));
        request.getRequestDispatcher("/Views/FeedBackListView.jsp").forward(request, response);
    }
}
