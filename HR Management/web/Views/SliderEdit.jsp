<%-- 
    Document   : ContractEdit
    Created on : Mar 06, 2022, 3:03:10 PM
    Author     : Egamorft
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Slider | Edit</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <!--<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="../css/style.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style>
            .box .slider{
                height: 40px;
                width: 300px;
                display: flex;
                align-items: center;
            }
            .box .slider .input{
                height: 10px;
                width: 100%;
                -webkit-appearance: none;
                outline: none;
                border-radius: 25px;
                box-shadow: inset 0px 0px 4px rgba(0,0,0,0.2);
            }
            .box .value{
                font-size: 30px;
                font-weight: 600;
                font-family: sans-serif;
                color: #3498db;
                width: 55px;
                text-align: center;
            }
        </style>
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../Home" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Home
            </a>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../userimg/${sessionScope.account.avatar}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>${sessionScope.account.fullname}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="../Slider/List">
                                <i class="fa fa-glass"></i> <span>Slider List</span>
                            </a>
                        </li>
                        <li>
                            <a href="../Slider/Add">
                                <i class="fa fa-glass"></i> <span>Slider Add</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div  class="col-lg-3"></div>
                    <div class="col-lg-6 ">
                        <section class="panel">
                            <header class="panel-heading text-center">
                                Edit Slider
                            </header>
                            <c:if test="${message != null}">
                                <c:choose>
                                    <c:when test = "${message eq 'Edit Slider Successfully!!'}">
                                        <div class="error alert alert-success" role="alert">
                                            <p class="mb-0">${message}</p>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="error alert alert-danger" role="alert">
                                            <p class="mb-0">${message}</p>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                                <c:remove var="message" scope="session" /> 
                            </c:if>
                            <div class="panel-body">
                                <form action="../Slider/Edit" method="POST">

                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                        <c:forEach items="${slider}" var="s">
                                            <input name="id" value="${s.id}" hidden="">
                                            <div class="col-lg-8">
                                                <div class="row ">
                                                    <div class="form-group col-lg-12">
                                                        <label for="id1">ID</label>
                                                        <input type="text" class="form-control" name="id1" value="${s.id}" disabled="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="fileName">Image</label>
                                                        </br>
                                                        <input multiple name="fileName" accept="image/jpg" id="file" type="file" onchange="preview()">
                                                        <br/>
                                                        <img id="frame" src="../sliderimg/${s.image}" width="100%"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="title">Title</label>
                                                        <input type="text" class="form-control" name="title" value="${s.title}" required="" pattern="^[a-zA-Z0-9)(]{2,20}$" oninvalid="this.setCustomValidity('Special Character or NULL error found!!')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="backlink">Back Link</label>
                                                        <input type="text" class="form-control" name="backlink" value="${s.backlink}" required="" pattern="[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)" oninvalid="this.setCustomValidity('Input must be an url !!')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="note">Note</label>
                                                        <textarea class="form-control" name="note" pattern="^[a-zA-Z0-9)(]{2,20}$" oninvalid="this.setCustomValidity('Special Character found!!')" oninput="this.setCustomValidity('')">${s.note}</textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label>Status</label><br/>
                                                        <c:choose>
                                                            <c:when test = "${p.status == 0}">
                                                                <span class="badge bg-yellow">Nonactive</span>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <span class="badge bg-green">Active</span>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                                <div class=" form-group row col-lg-12 text-center">
                                                    <button type="submit" id="submit-btn" class="btn btn-info">Save Change</button>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </form>

                            </div>
                        </section>
                    </div>
                </div>
            </section>

        </div>
        <!-- jQuery 2.0.2 -->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
        <script src="../js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="../js/Director/app.js" type="text/javascript"></script>
        <script src="../js/Director/myScript.js" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script>

                                                            /** HIDE ALERT**/
                                                            $(document).click(function (e) {
                                                                $('.error').hide();
                                                            });
                                                            /** HIDE ALERT**/

                                                            /***AVATAR SCRIPT***/
                                                            function preview() {
                                                                frame.src = URL.createObjectURL(event.target.files[0]);
                                                                var file = document.getElementById('file')
                                                                var bodyFormData = new FormData();
                                                                bodyFormData.append('fileName', file.files[0]);
                                                                axios({
                                                                    method: "post",
                                                                    url: "http://localhost:8080/HR_Management/UploadSlider",
                                                                    data: bodyFormData,
                                                                    headers: {"Content-Type": "multipart/form-data"},
                                                                }).then(function (response) {
                                                                    //handle success
                                                                    console.log(response);
                                                                })
                                                                        .catch(function (response) {
                                                                            //handle error
                                                                            console.log(response);
                                                                        });
                                                            }
                                                            /***AVATAR SCRIPT***/
        </script>
    </body>
</html>


