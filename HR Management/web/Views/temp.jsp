<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Timesheet List</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">

        <link href="../css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>


    </head>
    <body class="skin-black">
        <div id='wrap'>
            <div id='calendar'></div>
            <div style='clear:both'></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>

        <script>


        </script>
    </body>
</html>
