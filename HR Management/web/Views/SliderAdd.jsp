<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>


        <meta charset="UTF-8">
        <title>Slider | Add</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" />
        <!-- google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' />
        <!-- Theme style -->
        <link href="../css/style.css" rel="stylesheet"/>
        <link href="../css/dialog.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../Home" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Home
            </a>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <%@include file="Header/Treebar.jsp" %>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div  class="col-lg-3"></div>
                        <div class="col-lg-6 ">
                            <section class="panel">
                                <header class="panel-heading text-center">
                                    Add Slider
                                </header>
                                <c:if test="${message != null}">
                                    <c:choose>
                                        <c:when test = "${message eq 'Image input null !! Pls add one'}">
                                            <div class="error alert alert-danger" role="alert">
                                                <p class="mb-0">${message}</p>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="error alert alert-success" role="alert">
                                                <p class="mb-0">${message}</p>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:remove var="message" scope="session" /> 
                                </c:if>
                                <div class="panel-body">
                                    <form action="../Slider/Add" method="POST">
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="fileName">Image</label><i style="color: red; font-size: 20px">*</i>
                                                        </br>
                                                        <input multiple name="fileName" accept="image/*" id="file" type="file" onchange="preview()">
                                                        <br/>
                                                        <img id="frame" src="" width="100%"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="title">Title</label><i style="color: red; font-size: 20px">*</i>
                                                        <input type="text" class="form-control" name="title" required="" pattern="^[a-zA-Z0-9)(]{2,20}$" oninvalid="this.setCustomValidity('Special Character or NULL error found!!')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="backlink">Back Link</label><i style="color: red; font-size: 20px">*</i>
                                                        <input type="text" class="form-control" name="backlink" required="" pattern="[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)" oninvalid="this.setCustomValidity('Input must be an url !!')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="note">Note</label>
                                                        <textarea class="form-control" name="note" pattern="^[a-zA-Z0-9)(]{2,20}$" oninvalid="this.setCustomValidity('Special Character found!!')" oninput="this.setCustomValidity('')">${p.description}</textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <br/>
                                                        <label class="labels" for="status">Nonactive</label>&nbsp&nbsp<input type="radio" value="0" name="status" class="labels" checked="">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <br/>
                                                        <label class="labels" for="status">Active</label>&nbsp&nbsp<input type="radio" value="1" name="status" class="labels">
                                                    </div>
                                                </div>
                                                <br/>
                                                <div class=" form-group row col-lg-12 text-center">
                                                    <button type="submit" id="submit-btn" class="btn btn-info"  >Add Slider</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </section>
                        </div>
                    </div>
                </section>
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    </body>
    <script>

                                                            /** HIDE ALERT**/
                                                            $(document).click(function (e) {
                                                                $('.error').hide();
                                                            });
                                                            /** HIDE ALERT**/

                                                            function preview() {
                                                                frame.src = URL.createObjectURL(event.target.files[0]);
                                                                var file = document.getElementById('file')
                                                                var bodyFormData = new FormData();
                                                                bodyFormData.append('fileName', file.files[0]);
                                                                axios({
                                                                    method: "post",
                                                                    url: "http://localhost:8080/HR_Management/UploadSlider",
                                                                    data: bodyFormData,
                                                                    headers: {"Content-Type": "multipart/form-data"},
                                                                }).then(function (response) {
                                                                    //handle success
                                                                    console.log(response);
                                                                })
                                                                        .catch(function (response) {
                                                                            //handle error
                                                                            console.log(response);
                                                                        });
                                                            }
    </script>
</html>
