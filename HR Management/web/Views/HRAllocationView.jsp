<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>HR Allocation</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="../css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Home
            </a>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <%@include file="Header/Treebar.jsp" %>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <c:if test="${MessageError != null}">
                        <div class="alert alert-danger" role="alert">
                            ${MessageError}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${MessageSuccess != null}">
                        <div class="alert alert-success" role="alert">
                            ${MessageSuccess}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    Allocation
                                </header>
                                <!-- <div class="box-header"> -->
                                <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                                <!-- </div> -->
                                <div class="panel-body table-responsive">
                                    <div class="box-tools m-b-15">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <form action="" method="post" >
                                                    <div class="input-group">
                                                        <div class="btn btn-md btn-default" style="width: 150px; pointer-events: none;"><span>Search</span></div>
                                                        <input id="searchKey" type="text" name="searchKey" class="form-control input-md" style="width: 450px;" placeholder="Enter username/fullname to search" onclick="dateHideShow()"/>
                                                        <br>
                                                        <div id="advanced" style="display: none">
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-lg-2"></div>
                                                                <div class="col-lg-8">
                                                                    <div class="col-md-1"></div>
                                                                    <div class="col-md-7">
                                                                        <label class="text-left" for="fromDate" style="width: 150px;">From</label><br>
                                                                        <input type="date" class="form-control" id="fromDate" style="width: 200px;" name="fromDate">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label class="text-left" for="toDate" style="width: 150px;">To</label><br>
                                                                        <input type="date" class="form-control" id="toDate" style="width: 200px;" name="toDate">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-lg-2"></div>
                                                                <div class="col-lg-8">
                                                                    <div class="col-md-1"></div>
                                                                    <div class="col-md-7">
                                                                        <label class="text-left" for="projectFilter" style="width: 150px;">Project</label><br>
                                                                        <select class="form-control input-md" style="width: 200px;" name="projectFilter" id="projectFilter">
                                                                            <option value="">Choose project</option>
                                                                            <c:forEach var="project" items="${projects}">
                                                                                <option value="${project}">${project}</option>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label class="text-left" for="processFilter" style="width: 150px;">Group</label><br>
                                                                        <select class="form-control input-md" style="width: 200px;" name="groupFilter" id="groupFilter">
                                                                            <option value="">Choose group</option>
                                                                            <c:forEach var="group" items="${groups}">
                                                                                <option value="${group}">${group}</option>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addAllocation">New allocation</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Add -->
                                    <div class="modal fade" id="addAllocation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="../Allocation/Add" method="post">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="type" style="width: 150px;">Project</label><br>
                                                                <select class="form-control input-md" style="width: 150px;" name="project" id="project">
                                                                    <c:forEach var="project" items="${projects}">
                                                                        <option value="${project}">${project}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="type" style="width: 150px;">User ID</label><br>
                                                                <input type="text" class="form-control" id="user_id" style="width: 90px;" name="user_id" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="from" style="width: 150px;">From</label><br>
                                                                <input type="date" class="form-control" id="fromDate" style="width: 200px;" name="fromDate" required>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="to" style="width: 150px;">To</label><br>
                                                                <input type="date" class="form-control" id="toDate" style="width: 200px;" name="toDate" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="role" style="width: 150px;">Project Role</label><br>
                                                                <select class="form-control input-md" style="width: 150px;" name="role" id="role">
                                                                    <c:forEach var="role" items="${projectRoles}">
                                                                        <option value="${role.key}">${role.value}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="effort" style="width: 150px;">
                                                                    Effort: &nbsp; <span id="rangeval">50</span>%
                                                                </label>
                                                                <br>
                                                                <input type="range" value="50" class="form-range" id="effort" style="width: 220px; height: 40px;" name="effort" onInput="$('#rangeval').html($(this).val())"><br>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" class="btn btn-primary" value="Add">
                                                    </form>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Add -->
                                    <div class="panel-body" id="allocationTable">
                                        <table class="table table-hover">
                                            <tr>
                                                <th style="width: 10%">Project Code</th>
                                                <th style="width: 10%">Group</th>
                                                <th style="width: 10%">Username</th>
                                                <th style="width: 13%">Full Name</th>
                                                <th style="width: 13%">Project Role</th>
                                                <th style="width: 10%">From</th>
                                                <th style="width: 10%">To</th>
                                                <th style="width: 10%">Effort</th>
                                                <th style="width: 5%">Label</th>
                                                <th style="width: 10%"></th>
                                            </tr>
                                            <c:forEach var="allocation" items="${allocationList}" >
                                                <!-- Edit -->
                                                <div class="modal fade" id="edit${allocation.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <p class="text-bold">Project: &nbsp; ${allocation.project_code}</p>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <p class="text-bold">Group: &nbsp; ${allocation.user.group_code}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <p class="text-bold">User Name: &nbsp; ${allocation.user.username}</p>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <p class="text-bold">Full Name: &nbsp; ${allocation.user.fullname}</p>
                                                                    </div>
                                                                </div>
                                                                <form action="../Allocation/Edit?id=${allocation.id}&project=${allocation.project_code}" method="post">
                                                                    <div class="row">
                                                                        <div class="col-lg-6">
                                                                            <label class="text-left" for="role" style="width: 150px;">Role</label><br>
                                                                            <select class="form-control input-md" style="width: 150px;" name="role" id="role">
                                                                                <c:forEach var="role" items="${projectRoles}">
                                                                                    <c:if test="${allocation.project_role == role.key}">
                                                                                        <option value="${role.key}" selected>${role.value}</option>
                                                                                    </c:if>
                                                                                    <c:if test="${allocation.project_role != role.key}">
                                                                                        <option value="${role.key}">${role.value}</option>
                                                                                    </c:if>
                                                                                </c:forEach>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <label class="text-left" for="effort" style="width: 150px;">
                                                                                Effort: &nbsp; <span id="rangeval${allocation.id}">${allocation.effort}</span>%
                                                                            </label>
                                                                            <br>
                                                                            <input type="range" value="${allocation.effort}" class="form-range" id="effort" style="width: 220px;" name="effort" onInput="$('#rangeval${allocation.id}').html($(this).val())"><br>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-lg-6">
                                                                            <label class="text-left" for="fromDate" style="width: 150px;">From</label><br>
                                                                            <input type="date" value="${allocation.from2}" class="form-control" id="fromDate" style="width: 200px;" name="fromDate">
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <label class="text-left" for="toDate" style="width: 150px;">To</label><br>
                                                                            <input type="date" value="${allocation.to2}" class="form-control" id="toDate" style="width: 200px;" name="toDate">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <label class="text-left" for="note" style="width: 150px;">Note</label><br>
                                                                            <textarea type="text" class="form-control" id="note" name="note" style="width: 100%; height: 150px;" required>${allocation.notes}</textarea>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="submit" class="btn btn-primary" value="Edit">
                                                                </form>
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <tr id="allocation${allocation.id}">
                                                    <td>${allocation.project_code}</td>
                                                    <td>${allocation.user.group_code}</td>
                                                    <td>${allocation.user.username}</td>
                                                    <td>${allocation.user.fullname}</td>
                                                    <td>${projectRoles[allocation.project_role]}</td>
                                                    <td>${allocation.from}</td>
                                                    <td>${allocation.to}</td>
                                                    <td> 
                                                        <div class="progress progress-striped progress-sm active">
                                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="${allocation.effort}" aria-valuemin="0" aria-valuemax="100" style="width: ${allocation.effort}%">
                                                                <span class="sr-only">${allocation.effort} Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green">${allocation.effort}%</span></td>
                                                    <td>
                                                        <a href="#" class="btn btn-md btn-default"  data-toggle="modal" data-target="#edit${allocation.id}"><i class="fa fa-pencil"></i></a>
                                                    </td>
                                                    </td>
                                                </tr>
                                                <!-- /Edit -->
                                            </c:forEach>
                                        </table>
                                        <div class="table-foot" id="table-foot">
                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                <c:if test="${total !=0 }">
                                                    <c:if test="${currentNumber>1}">
                                                        <li><button  class="btn btn-sm btn-default" onclick="page(${currentNumber-1})"><<</button></li>
                                                        </c:if>
                                                        <c:if test="${currentNumber>3}">
                                                        <li><button class="btn btn-sm btn-default" onclick="page(${1})">1</button></li>
                                                        <li><button  class="btn btn-sm btn-default" onclick="page(${Math.max(end-5,1)})">...</button></li>
                                                        </c:if>
                                                        <c:forEach begin="${begin}" end="${end}" var="num">
                                                            <c:if test="${num == currentNumber}">
                                                            <li><button id="page-active" class="btn btn-sm btn-primary" onclick="page(${num})">${num}</button></li>
                                                            </c:if>
                                                            <c:if test="${num != currentNumber}">
                                                            <li><button  class="btn btn-sm btn-default" onclick="page(${num})">${num}</button></li>
                                                            </c:if>    
                                                        </c:forEach>

                                                    <c:if test="${begin+3 < total}">
                                                        <li><button  class="btn btn-sm btn-default" onclick="page(${begin+3})">...</button></li>
                                                        <li><button  class="btn btn-sm btn-default" onclick="page(${total})">${total}</button></li>
                                                        </c:if>
                                                        <c:if test="${begin+3 >= total && end != total}">
                                                        <li><button  class="btn btn-sm btn-default" onclick="page(${total})">${total}</button></li>
                                                        </c:if>

                                                    <c:if test="${currentNumber < total}">
                                                        <li><button  class="btn btn-sm btn-default" onclick="page(${currentNumber+1})">>></button></li>  
                                                        </c:if>
                                                    </c:if>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <script src="../js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="../js/Director/app.js" type="text/javascript"></script>
        <script>

                                                            function dateHideShow() {
                                                                var x = document.getElementById("advanced");
                                                                if (x.style.display === "none") {
                                                                    x.style.display = "block";
                                                                } else {
                                                                    x.style.display = "none";
                                                                }
                                                            }

                                                            function page(number) {
                                                                var pageNumber = number;
                                                                var fromDate = document.getElementById('fromDate').value;
                                                                var toDate = document.getElementById('toDate').value;
                                                                var group = document.getElementById('groupFilter').value;
                                                                var project = document.getElementById('projectFilter').value;
                                                                var searchKey = document.getElementById('searchKey').value;
                                                                console.log(searchKey.split(" ").join("-"));
                                                                var link = "http://localhost:8080/HR_Management/Allocation/AllocationList?";
                                                                link += "page=" + pageNumber;
                                                                link += "&";
                                                                link += "fromDate=" + fromDate;
                                                                link += "&";
                                                                link += "toDate=" + toDate;
                                                                link += "&";
                                                                link += "group=" + group;
                                                                link += "&";
                                                                link += "project=" + project;
                                                                link += "&";
                                                                link += "searchKey=" + searchKey.split(" ").join("-");
                                                                $('#allocationTable').load(link + " " + "#allocationTable");
                                                            }

                                                            $(document).ready(function () {
                                                                $('#fromDate').change(function () {
                                                                    page(1);
                                                                });
                                                                $('#toDate').change(function () {
                                                                    page(1);
                                                                });
                                                                $('#projectFilter').change(function () {
                                                                    page(1);
                                                                });
                                                                $('#groupFilter').change(function () {
                                                                    page(1);
                                                                });
                                                                $('#searchKey').keyup(function () {
                                                                    page(1);
                                                                });

//                                                                setInterval(function () {
//                                                                    var number = document.getElementById('page-active').innerHTML;
//                                                                    console.log(number);
//                                                                    page(number);
//                                                                }, 10000);
                                                            });


        </script>
    </body>
</html>
