<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>


        <meta charset="UTF-8">
        <title>Director | Simple Tables</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" />
        <!-- google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' />
        <!-- Theme style -->
        <link href="../css/style.css" rel="stylesheet"/>
        <link href="../css/dialog.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="Views/Home.jsp" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Home
            </a>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <%@include file="Header/Treebar.jsp" %>
            <aside class="right-side">
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="panel">
                            <header class="panel-heading">
                                Absence Request List
                            </header>
                            <div class="panel-body table-responsive">
                                <div class="box-tools m-b-15">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <form action="" method="post" >
                                                <div class="input-group">
                                                    <div class="btn btn-md btn-default" style="width: 150px; pointer-events: none;"><span>Search by Title</span></div>
                                                    <input id="title" type="text" name="title" class="form-control input-md" style="width: 450px;" placeholder="Enter title to search"/>
                                                    <br>
                                                    <div id="advanced">
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-lg-2"></div>
                                                            <div class="col-lg-8">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-7">
                                                                    <label class="text-left" for="from" style="width: 150px;">From</label><br>
                                                                    <input type="date" class="form-control" id="fromDate" style="width: 200px;" name="fromDate">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label class="text-left" for="to" style="width: 150px;">To</label><br>
                                                                    <input type="date" class="form-control" id="toDate" style="width: 200px;" name="toDate">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-lg-2"></div>
                                                            <div class="col-lg-8">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-7">
                                                                    <label class="text-left" for="type" style="width: 150px;">Type</label><br>
                                                                    <select class="form-control input-md" style="width: 200px;" name="type" id="type">
                                                                        <option value="">All Type</option>
                                                                        <c:forEach var="type" items="${AbsenceType}">
                                                                            <option value="${type.key}">${type.value}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label class="text-left" for="status" style="width: 150px;">Status</label><br>
                                                                    <select class="form-control input-md" style="width: 200px;" name="status" id="status">
                                                                        <option value="">All Status</option>
                                                                        <c:forEach var="status" items="${AbsenceStatus}">
                                                                            <option value="${status.key}">${status.value}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addAbsence">Add new Absence Request</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body" id="absenceTable">
                                    <!-- Add -->
                                    <div class="modal fade" id="addAbsence" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="../AbsencesController/Add" method="post">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="type" style="width: 150px;">Type</label><br>
                                                                <select class="form-control input-md" style="width: 150px;" name="type" id="type">
                                                                    <c:forEach var="type" items="${AbsenceType}">
                                                                        <option value="${type.key}">${type.value}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="type" style="width: 150px;">Duration</label><br>
                                                                <input type="text" class="form-control" id="duration" style="width: 90px;" name="duration">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="from" style="width: 150px;">From</label><br>
                                                                <input type="date" class="form-control" id="fromDate" style="width: 200px;" name="fromDate">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="text-left" for="to" style="width: 150px;">To</label><br>
                                                                <input type="date" class="form-control" id="toDate" style="width: 200px;" name="toDate">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label class="text-left" for="type" style="width: 150px;">Title</label><br>
                                                                <textarea type="text" class="form-control" id="title" name="title" style="width: 100%; height: 150px;"></textarea>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" class="btn btn-primary" value="Add">
                                                    </form>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Request Date</th>
                                            <th>Type</th>
                                            <th style="width: 300px;">Title</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Duration</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        <c:forEach items="${listA}" var="a">
                                            <!-- Edit -->
                                            <div class="modal fade" id="edit${a.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../AbsencesController/Edit?id=${a.id}" method="post">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <label class="text-left" for="type" style="width: 150px;">Type</label><br>
                                                                        <select class="form-control input-md" style="width: 150px;" name="type" id="type">
                                                                            <c:forEach var="type" items="${AbsenceType}">
                                                                                <c:if test="${a.type == type.key}">
                                                                                    <option value="${type.key}" selected>${type.value}</option>
                                                                                </c:if>
                                                                                <c:if test="${a.type != type.key}">
                                                                                    <option value="${type.key}">${type.value}</option>
                                                                                </c:if>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <label class="text-left" for="type" style="width: 150px;">Duration</label><br>
                                                                        <input type="text" value="${a.duration}" class="form-control" id="duration" style="width: 90px;" name="duration">
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <label class="text-left" for="from" style="width: 150px;">From</label><br>
                                                                        <input type="date" value="${a.from}" class="form-control" id="fromDate" style="width: 200px;" name="fromDate">
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <label class="text-left" for="to" style="width: 150px;">To</label><br>
                                                                        <input type="date" value="${a.to}" class="form-control" id="toDate" style="width: 200px;" name="toDate">
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <label class="text-left" for="type" style="width: 150px;">Title</label><br>
                                                                        <textarea type="text" class="form-control" id="title" name="title" style="width: 100%; height: 150px;">${a.title}</textarea>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="submit" class="btn btn-primary" value="Edit">
                                                            </form>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- View -->
                                            <div class="modal fade" id="view${a.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <p class="text-bold">Request date: &nbsp; ${a.request_date}</p>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <p class="text-bold">Duration: &nbsp; ${a.duration}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <p class="text-bold">From: &nbsp; ${a.from}</p>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <p class="text-bold">To: &nbsp; ${a.to}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <p class="text-bold">Type: &nbsp; ${AbsenceType[a.type]}</p>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label>Status:</label>
                                                                    <c:if test = "${a.status == 1}">
                                                                        <p class="text-info text-bold">${AbsenceStatus[a.status]}</p>
                                                                    </c:if>
                                                                    <c:if test = "${a.status == 2}">
                                                                        <p class="text-success text-bold">${AbsenceStatus[a.status]}</p>
                                                                    </c:if>
                                                                    <c:if test = "${a.status == 3}">
                                                                        <p class="text-danger text-bold">${AbsenceStatus[a.status]}</p>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <p class="text-bold">Title: &nbsp; ${a.title}</p>
                                                                    <c:if test="${a.status == 3}">
                                                                        <p class="text-danger text-bold">Reason: &nbsp; ${a.reject_reason}</p>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <tr>
                                                <td>${a.request_date}</td>
                                                <td>${AbsenceType[a.type]}</td>
                                                <td>${a.title}</td>
                                                <td>${a.from}</td>
                                                <td>${a.to}</td>
                                                <td>${a.duration}h</td>
                                                <td>
                                                    <c:if test = "${a.status == 1}">
                                                        <span class="badge bg-aqua">${AbsenceStatus[a.status]}</span>
                                                    </c:if>
                                                    <c:if test = "${a.status == 2}">
                                                        <span class="badge bg-green">${AbsenceStatus[a.status]}</span>
                                                    </c:if>
                                                    <c:if test = "${a.status == 3}">
                                                        <span class="badge bg-red">${AbsenceStatus[a.status]}</span>
                                                    </c:if>
                                                </td>
                                                <td>
                                                    <c:if test="${a.status == 1 || a.status == 3}">
                                                        <a class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#edit${a.id}"></a>&nbsp;&nbsp;
                                                    </c:if>
                                                    <c:if test= "${a.status != 1}">
                                                        <a class="glyphicon glyphicon-eye-open" data-toggle="modal" data-target="#view${a.id}"></a>&nbsp;&nbsp;
                                                    </c:if>
                                                    <c:if test="${a.status == 1}">
                                                        <a id="delete" onclick="deleteAbsence(${a.id})" href="#" class="glyphicon glyphicon-trash"></a>&nbsp;&nbsp;
                                                    </c:if>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                    <div class="table-foot">
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <c:forEach begin="1" end="${endP}" var="p">
                                                <li><a id="page-active" href="#" onclick="page(${p})">${p}</a></li>
                                                </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
        <script type="text/javascript"></script>
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <script src="../js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="../js/Director/app.js" type="text/javascript"></script>
        <script>
                                                    const replaceWhitespace = (str = '') => {
                                                        let res = '';
                                                        const {length} = str;
                                                        for (let i = 0; i < length; i++) {
                                                            const char = str[i];
                                                            if (!(char === ' ')) {
                                                                res += char;
                                                            } else {
                                                                res += '_';
                                                            }
                                                            ;
                                                        }
                                                        ;
                                                        return res;
                                                    };

                                                    function page(number) {
                                                        var pageNumber = number;
                                                        var fromDate = document.getElementById('fromDate').value;
                                                        var toDate = document.getElementById('toDate').value;
                                                        var status = document.getElementById('status').value;
                                                        var type = document.getElementById('type').value;
                                                        var title = document.getElementById('title').value;
                                                        var link = "http://localhost:8080/HR_Management/AbsencesController/Absence?";
                                                        link += "page=" + pageNumber;
                                                        link += "&";
                                                        link += "fromDate=" + fromDate;
                                                        link += "&";
                                                        link += "toDate=" + toDate;
                                                        link += "&";
                                                        link += "status=" + status;
                                                        link += "&";
                                                        link += "type=" + type;
                                                        link += "&";
                                                        link += "title=" + replaceWhitespace(title);
                                                        $('#absenceTable').load(link + " " + "#absenceTable");
                                                    }

                                                    function deleteAbsence(id) {
                                                        var cf = confirm("Are you sure to delete?");
                                                        var pageNumber = document.getElementById('page-active').innerHTML;
                                                        var fromDate = document.getElementById('fromDate').value;
                                                        var toDate = document.getElementById('toDate').value;
                                                        var status = document.getElementById('status').value;
                                                        var type = document.getElementById('type').value;
                                                        var title = document.getElementById('title').value;
                                                        if (cf) {
                                                            //Logic to delete the item
                                                            $.ajax({

                                                                type: "POST",

                                                                url: "http://localhost:8080/HR_Management/AbsencesController/Delete",

                                                                data: {id: id,
                                                                    fromDate: fromDate,
                                                                    toDate: toDate,
                                                                    status: status,
                                                                    type: type,
                                                                    title: title,
                                                                    page: pageNumber},

                                                                success: function (number) {
                                                                    page(number);
                                                                }

                                                            });


                                                        }
                                                    }


                                                    $(document).ready(function () {
                                                        $('#fromDate').change(function () {
                                                            page(1);
                                                        });
                                                        $('#toDate').change(function () {
                                                            page(1);
                                                        });
                                                        $('#status').change(function () {
                                                            page(1);
                                                        });
                                                        $('#type').change(function () {
                                                            page(1);
                                                        });
                                                        $('#title').keyup(function () {
                                                            page(1);
                                                        });

//                                                                setInterval(function () {
//                                                                    var number = document.getElementById('page-active').innerHTML;
//                                                                    console.log(number);
//                                                                    page(number);
//                                                                }, 10000);
                                                    });
        </script>
    </body>
</html>
